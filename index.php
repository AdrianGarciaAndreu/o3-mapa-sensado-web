<?php
/**
 * Redireccion base a la aplicacion
 * @author Adrian Garcia Andreu
 * @version 1.1
 */
header('Location: ./app/');

?>