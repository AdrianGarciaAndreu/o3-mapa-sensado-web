

// Direccion del servidor a la API del servidor
const servidorAPI = "http://localhost/proyecto3A/api/v1.0/";




// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getMedidas()
// --------------------------------------------------------------
function getMedidas(){

   var recurso = servidorAPI+"medidas/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){
        //console.log(datos["datos"]); 
        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        procesarMedidas(datos["datos"]);
    });
} 


// --------------------------------------------------------------
// --------------------------------------------------------------
// Recibe un listado de medidas (en formato JSON)
// se disponen en una tabla y se muestran en la pagina 
// --------------------------------------------------------------
// [Medidas] --> procesarMedidas()
// --------------------------------------------------------------
function procesarMedidas(medidas){

    //console.log(typeof(medidas));
    //console.log(medidas);

    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_datos_medidas");
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-responsive table-hover">`;
    visualizacion += `<thead class="thead-light">`;
        visualizacion += `<th class="px-lg-5">Fecha</th>`;
        visualizacion += `<th class="px-lg-5">Tipo de gas</th>`;
        visualizacion += `<th class="px-lg-5">Valor</th>`;
        visualizacion += `<th class="px-lg-5">Latitud</th>`;
        visualizacion += `<th class="px-lg-5">Longitud</th>`;
    visualizacion += `</thead>`;

    
    // se crea una fila por cada medida
    for (const medida of medidas) {
        //console.log(medida);
        visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${medida["momento"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["tipoMedida"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["valor"]} ug/m<sup>3</sup></td>`;
            visualizacion += `<td class="px-lg-5">${medida["latitud"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["longitud"]}</td>`;

        visualizacion += `</tr>`;
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;


}




// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// --------------------------------------------------------------
// [Medidas] --> procesarMedidas()
// --------------------------------------------------------------
function iniciarPeticionesAlServidor(){
    window.onload = function(){ // cuando el navegador ha cargado, inicio las peticiones
        getMedidas();
    }
}


