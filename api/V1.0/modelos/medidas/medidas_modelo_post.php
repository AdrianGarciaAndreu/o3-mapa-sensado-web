<?php

/**
 * Inserta una nueva medida en la BBDD
 * @author Adrian Garcia Andreu
 * @version 1.1
 */

$sql = "INSERT INTO medidas(valor, latitud, longitud, momento, tipoMedida) ";
$sql .= "VALUES(".$form_params["valor"].", ".$form_params["latitud"].", ".$form_params["longitud"].", ";
$sql .= "NOW() , '".$form_params["tipoMedida"]."')";

$respuesta = mysqli_query($conexion, $sql);
